class Article < ActiveRecord::Base
  belongs_to :user # completes one to many association
  has_many :article_categories
  has_many :categories, through: :article_categories

  #ensure that title and description aren't nil
  #and that length has to be obeyed
  validates :title, presence: true, length: {minimum: 3, maximum: 50}
  validates :description, presence: true, length: {minimum: 10, maximum: 300}
  validates :user_id, presence: true
end